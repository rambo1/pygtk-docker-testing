######################
# Base builder image #
######################
FROM ubuntu:18.04 as builder_base

ENV \
  # locale
  LC_ALL=C.UTF-8 \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.0.5


RUN apt-get update && apt-get install -y \
        curl \
        gzip \
        tar \
        python3 \
        python3-dev \
        python3-pip \
        bash \
        git-core \
        libssl-dev \
        libffi-dev \
        libzmq3-dev \
        build-essential \
        python3-gi \
        gir1.2-glib-2.0 \
        gir1.2-gtk-3.0 \
        xvfb \
        software-properties-common \
    # Installing `poetry` package manager:
    # https://github.com/python-poetry/poetry
    && pip3 install "poetry==$POETRY_VERSION" \
    && poetry config virtualenvs.create false \
    # Some precommit hooks need python3.7
    && add-apt-repository ppa:deadsnakes/ppa -y \
    && apt-get install -y \
        python3.7 \
        python3.7-dev \
    && true

SHELL ["/bin/bash", "-c"]


# Copy only requirements, to cache them in docker layer:
WORKDIR /pysetup
COPY ./poetry.lock ./pyproject.toml /pysetup/
# Install basic requirements
# But only after https://github.com/python-poetry/poetry/issues/2102 is fixed
#RUN poetry config virtualenvs.create false \
#  && poetry install --no-dev --no-interaction --no-ansi


#####################################
# Base stage for development builds #
#####################################
FROM builder_base as devel_build
# Install deps, docker is already isolated so we don't need virtualenv
WORKDIR /pysetup
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi


###########
# Hacking #
###########
FROM devel_build as devel_shell
COPY . /code
WORKDIR /code
# Re run install to get the service itself installed
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi \
  && true
ENTRYPOINT ["/bin/bash"]


#############
# Run tests #
#############
FROM devel_build as test
COPY . /code
WORKDIR /code
ENTRYPOINT ["docker/entrypoint-test.sh"]
# Re run install to get the service itself installed
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi \
  && docker/pre_commit_init.sh \
  && true
