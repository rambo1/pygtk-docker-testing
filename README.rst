=======
gtktest
=======

Test GTK

Docker
------

This depends on GObject and GTK libraries etc from the operating system level, easiest way
to get hacking is to build the docker image and work inside it.

Passing X11 to Docker on OSX
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need XQuartz (brew cask install xquartz), and since the actual Docker engine runs in a
VM you need to allow connections over network (disabled by default, enable in settings and restart
XQuartz), this you need to do only once.

Then you need authorize connections from localhost (xhost + 127.0.0.1), this you need to do after every XQuartz
restart. Finally you need to set the DISPLAY ENV variable correctly when creating the container (done below),
for copy-pasting commands as-is export the following::

    export DOCKER_DISPLAY="-e DISPLAY=host.docker.internal:0"

Passing X11 to Docker on Linux
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here we can get away with just passing the DISPLAY ENV variable as-is (you still need to have that
exported so run these from a GUI terminal or know more about what you are doing), but we do need to use host networking
again for copy-pasting commands as-is export the following::

    export DOCKER_DISPLAY="--env=DISPLAY --net=host"

Creating the container
^^^^^^^^^^^^^^^^^^^^^^

Make sure you have defined DOCKER_DISPLAY above.

Build image, create container and start it::

    docker build --target devel_shell -t gtktest:devel_shell .
    docker create --name gtktest_devel -p 53190:53190 -it -v `pwd`":/code" `echo $DOCKER_DISPLAY` --volume="$HOME/.Xauthority:/root/.Xauthority:rw" gtktest:devel_shell
    docker start -i gtktest_devel

This will give you a shell with system level dependencies installed, you should do any shell things (like
run tests, pre-commit checks etc) there.

pre-commit considerations
^^^^^^^^^^^^^^^^^^^^^^^^^

If working in Docker instead of native env you need to run the pre-commit checks in docker too::

    docker exec -i gtktest_devel /bin/bash -c "pre-commit install"
    docker exec -i gtktest_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above.

Test suite
^^^^^^^^^^

You can use the devel shell to run py.test when doing development, for CI use
the "test" target in the Dockerfile::

    docker build --target test -t gtktest:test .
    docker run --rm -v `pwd`":/code" gtktest:test


Development
-----------

TLDR:

- Create and activate a Python 3.7 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.7` my_virtualenv

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    pre-commit install
    pre-commit run --all-files

- Ready to go, try the following::

    gtktest --defaultconfig >config.toml
    gtktest -vv config.toml

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).

Running "pre-commit run --all-files" and "py.test -v" regularly during development and
especially before committing will save you some headache.
