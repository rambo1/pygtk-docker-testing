"""Default configuration file contents"""

# Remember to add tests for keys into test_gtktest.py
DEFAULT_CONFIG_STR = """
[zmq]
pub_sockets = ["ipc:///tmp/gtktest_pub.sock", "tcp://*:53190"]
""".lstrip()
