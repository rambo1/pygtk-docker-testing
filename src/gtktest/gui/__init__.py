"""The GUI stuff separated to a submodule"""
from typing import List, Any
import asyncio
import logging
import gi  # type: ignore

gi.require_version("Gtk", "3.0")
# pylint: disable=C0413
from gi.repository import Gtk  # type: ignore
from datastreamcorelib.datamessage import PubSubDataMessage
from gobjectservicelib.service import SimpleService

LOGGER = logging.getLogger(__name__)


class MainWindow(Gtk.Window):  # type: ignore
    """Root of our GUI"""

    def __init__(self, service: SimpleService, aioloop: "asyncio.AbstractEventLoop") -> None:
        """Initialize"""
        self.service = service
        self.aioloop = aioloop
        Gtk.Window.__init__(self, title="GTK DataStream App Test")
        self.set_default_size(600, 350)
        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.create_textview()
        self.create_toolbar()

    def create_toolbar(self) -> None:
        """Create the toolbar with buttons"""
        toolbar = Gtk.Toolbar()
        self.grid.attach(toolbar, 0, 0, 3, 1)

        btn = Gtk.ToolButton()
        btn.set_icon_name("network-transmit-symbolic")
        toolbar.insert(btn, 0)

        def click_callback(*args: List[Any]) -> None:
            """Button click callback, send a datamessage"""
            nonlocal self
            LOGGER.debug("Called with {}".format(args))
            msg = PubSubDataMessage(topic="button1_click")
            self.aioloop.call_soon_threadsafe(self.service.psmgr.publish, msg)

        btn.connect("clicked", click_callback)

    def create_textview(self) -> None:
        """Add scrollable textview to the main grid"""
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()
        scrolledwindow.add(self.textview)
