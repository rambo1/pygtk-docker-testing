"""Main classes for gtktest"""
import asyncio  # pylint: disable=W0611
from typing import cast, Optional, Any, List, Dict  # pylint: disable=W0611
from dataclasses import dataclass, field
import logging


from datastreamcorelib.pubsub import PubSubMessage, Subscription
from datastreamcorelib.datamessage import PubSubDataMessage
from gobjectservicelib.service import SimpleService

from .gui import MainWindow

LOGGER = logging.getLogger(__name__)


@dataclass
class GTKTestService(SimpleService):
    """Main class for gtktest"""

    _sendertask: "Optional[asyncio.Task[Any]]" = field(init=False, default=None)
    _gtk_roots: Dict[str, Any] = field(init=False, default_factory=dict)

    def __post_init__(self) -> None:
        """Setup the GUI base"""
        win = MainWindow(self, asyncio.get_event_loop())

        def mainwindow_destroy(*args: List[Any]) -> None:
            """Handle the window close event, call quit"""
            LOGGER.debug("Called with {}".format(args))
            nonlocal self
            self.quit(0)

        win.connect("destroy", mainwindow_destroy)
        self._gtk_roots["window"] = win

    async def run(self) -> int:
        """Show the window and pass control to super"""
        self._gtk_roots["window"].show_all()
        return await super().run()

    def reload(self) -> None:
        """Load configs, restart sockets"""
        super().reload()
        # Do something

        # Example subscription for receiving messages (specifically DataMessages)
        sub = Subscription(
            self.config["zmq"]["pub_sockets"][0],  # Listen to our own heartbeat
            "HEARTBEAT",
            self.example_success_callback,
            decoder_class=PubSubDataMessage,
            # This is just an example, don't pass metadata you don't intent to use
            metadata={"somekey": "somevalue"},
        )
        self.psmgr.subscribe(sub)

    def example_success_callback(self, sub: Subscription, msg: PubSubMessage) -> None:
        """Callback for the example subscription"""
        # We know it's actually datamessage but the broker deals with the parent type
        msg = cast(PubSubDataMessage, msg)
        LOGGER.info("Got {} (sub.metadata={})".format(msg, sub.metadata))
        end_iter = self._gtk_roots["window"].textbuffer.get_end_iter()
        self._gtk_roots["window"].textbuffer.insert(end_iter, "{}\n".format(msg))

        outmsg = PubSubDataMessage(topic="bartopic")
        self.psmgr.publish(outmsg)
